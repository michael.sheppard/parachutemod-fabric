/*
 * BipedEntityModelMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.MathHelper;
import net.parachutemod.common.entity.ParachuteEntity;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Environment(EnvType.CLIENT)
@Mixin(BipedEntityModel.class)
public class BipedEntityModelMixin {
    @Final @Shadow public ModelPart rightLeg;
    @Final @Shadow public ModelPart leftLeg;

    @Inject(at = @At(value = "TAIL"),  method = "setAngles*")
    public void setRiderSitting(LivingEntity livingEntity, float f, float g, float h, float i, float j, CallbackInfo info) {
        boolean bl = livingEntity.getRoll() > 4;
        float k = 1.0F;
        if (bl) {
            k = (float)livingEntity.getVelocity().lengthSquared();
            k /= 0.2F;
            k *= k * k;
        }

        if (k < 1.0F) {
            k = 1.0F;
        }
        if (livingEntity instanceof PlayerEntity && livingEntity.getVehicle() instanceof ParachuteEntity chute) {
            if (chute.shouldRiderSit()) {
                rightLeg.pitch = -1.4137167F;
                rightLeg.yaw = 0.31415927F;
                rightLeg.roll = 0.07853982F;
                leftLeg.pitch = -1.4137167F;
                leftLeg.yaw = -0.31415927F;
                leftLeg.roll = -0.07853982F;
            } else {
                rightLeg.pitch = MathHelper.cos(f * 0.6662F) * 1.4F * g / k;
                leftLeg.pitch = MathHelper.cos(f * 0.6662F + 3.1415927F) * 1.4F * g / k;
                rightLeg.yaw = 0.0F;
                leftLeg.yaw = 0.0F;
                rightLeg.roll = 0.0F;
                leftLeg.roll = 0.0F;
            }
        }
    }

}
