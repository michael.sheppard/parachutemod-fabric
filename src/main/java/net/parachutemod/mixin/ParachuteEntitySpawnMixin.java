/*
 * ParachuteModMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.EntityType;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.parachutemod.common.entity.ParachuteEntity;
import net.parachutemod.common.ParachuteMod;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Environment(EnvType.CLIENT)
@Mixin(ClientPlayNetworkHandler.class)
public abstract class ParachuteEntitySpawnMixin {
	@Shadow	private ClientWorld world;

	@Inject(at = @At(value = "TAIL"),  method = "onEntitySpawn")
	private void addParachute(EntitySpawnS2CPacket packet, CallbackInfo info) {
		double x = packet.getX();
		double y = packet.getY();
		double z = packet.getZ();
		EntityType<?> entityType = packet.getEntityType();
		ParachuteEntity chute = null;
		if (entityType == ParachuteMod.PARACHUTE) {
			chute = new ParachuteEntity(ParachuteMod.PARACHUTE, world);
		}
		if (chute != null) {
			int id = packet.getId();
			chute.updateTrackedPosition(x, y, z);
			chute.refreshPositionAfterTeleport(x, y, z);
			chute.setPitch(packet.getPitch());
			chute.setYaw(packet.getYaw());
			chute.setId(id);
			chute.setUuid(packet.getUuid());
			world.addEntity(chute);
		}
	}
}
