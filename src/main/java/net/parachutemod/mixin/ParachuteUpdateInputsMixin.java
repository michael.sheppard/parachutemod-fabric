/*
 * ParachuteUpdateInputsMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.mixin;


import com.mojang.authlib.GameProfile;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.input.Input;
import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.world.ClientWorld;
import net.parachutemod.common.entity.ParachuteEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Environment(EnvType.CLIENT)
@Mixin(ClientPlayerEntity.class)
public abstract class ParachuteUpdateInputsMixin extends AbstractClientPlayerEntity {
    @Shadow public Input input;
    @Shadow private boolean riding;

    public ParachuteUpdateInputsMixin(ClientWorld world, GameProfile profile) {
        super(world, profile);
    }

    @Inject(at = @At(value = "TAIL"),  method = "tickRiding")
    private void UpdateInputs(CallbackInfo info) {
        if (getVehicle() instanceof ParachuteEntity parachuteEntity) {
            parachuteEntity.updateInputs(input.pressingLeft, input.pressingRight, input.pressingForward, input.pressingBack, input.jumping);
            riding |= input.pressingLeft || input.pressingRight || input.pressingForward || input.pressingBack || input.jumping;
        }
    }
}
