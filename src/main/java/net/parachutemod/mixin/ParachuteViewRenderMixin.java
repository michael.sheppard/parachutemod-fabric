/*
 * ParachuteViewRenderMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RotationAxis;
import net.parachutemod.common.ModConstants;
import net.parachutemod.common.entity.ParachuteEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Environment(EnvType.CLIENT)
@Mixin(GameRenderer.class)
public abstract class ParachuteViewRenderMixin {
    @Unique private float rollAngle = 0.0f;
    @Unique private final MinecraftClient CLIENT = MinecraftClient.getInstance();

    @Inject(at = @At(value = "HEAD"),  method = "renderWorld")
    public void parachutemod$renderParachuteRoll(float tickDelta, long limitTime, MatrixStack matrix, CallbackInfo info) {
        boolean leftTurn;
        boolean rightTurn;
        double forwardMotion;
        boolean firstPerson = CLIENT.options.getPerspective().isFirstPerson();

        if (CLIENT.player != null) {
            if (CLIENT.player.getVehicle() instanceof ParachuteEntity) {
                if (!((ParachuteEntity)CLIENT.player.getVehicle()).getShowRollEffect()) {
                    return;
                }
                leftTurn = ParachuteEntity.leftTurn;
                rightTurn = ParachuteEntity.rightTurn;
                forwardMotion = ParachuteEntity.forwardMotion;

                if (leftTurn && forwardMotion > ModConstants.SLIDE_MOMENTUM) {
                    if (!firstPerson) {
                        if (rollAngle < 0.0f) {
                            rollAngle++;
                        }
                        rollAngle++;
                    } else {
                        if (rollAngle > 0.0f) {
                            rollAngle--;
                        }
                        rollAngle--;
                    }
                }
                if (leftTurn && forwardMotion < ModConstants.TURN_MOMENTUM) {
                    if (rollAngle < 0.0f) {
                        rollAngle++;
                    }
                    if (rollAngle > 0.0f) {
                        rollAngle--;
                    }
                }
                if (rightTurn && forwardMotion > ModConstants.SLIDE_MOMENTUM) {
                    if (!firstPerson) {
                        if (rollAngle > 0.0f) {
                            rollAngle--;
                        }
                        rollAngle--;
                    } else {
                        if (rollAngle < 0.0f) {
                            rollAngle++;
                        }
                        rollAngle++;
                    }
                }
                if (rightTurn && forwardMotion < ModConstants.TURN_MOMENTUM) {
                    if (rollAngle < 0.0f) {
                        rollAngle++;
                    }
                    if (rollAngle > 0.0f) {
                        rollAngle--;
                    }
                }
                if (!leftTurn && !rightTurn) {
                    if (rollAngle < 0.0f) {
                        rollAngle++;
                    } else if (rollAngle > 0.0f) {
                        rollAngle--;
                    } else {
                        rollAngle = 0.0f;
                    }
                }

                rollAngle = MathHelper.clamp(rollAngle, -ModConstants.MAX_ROLL_ANGLE, ModConstants.MAX_ROLL_ANGLE);
                matrix.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(rollAngle));
            }
        }
    }

}
