/*
 * ParachutePlayerEntityTickMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.mixin;


import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.parachutemod.common.entity.ParachuteEntity;
import net.parachutemod.common.item.ParachuteItem;
import net.parachutemod.common.ParachuteMod;
import net.parachutemod.common.item.ParachutePackItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(PlayerEntity.class)
public abstract class ParachutePlayerEntityTickMixin {

    @Inject(at = @At(value = "TAIL"),  method = "tick")
    private void playerTickEvent(CallbackInfo info) {
        autoActivateDevice((PlayerEntity)(Object) this);
        togglePlayerParachutePack((PlayerEntity) (Object) this);
    }

    // Check the players currently held item and if it is a
    // PARACHUTE_ITEM set a PARACHUTEPACK_ITEM in the chestplate armor slot.
    // Remove the PARACHUTEPACK_ITEM if the player is no longer holding the PARACHUTE_ITEM
    // as long as the player is not on the PARACHUTE. If there is already an
    // armor item in the armor slot do nothing.
    @Unique
    private void togglePlayerParachutePack(PlayerEntity player) {
        if (player != null) {
            ItemStack armor = player.getEquippedStack(EquipmentSlot.CHEST);
            boolean deployed = player.getVehicle() instanceof ParachuteEntity;
            boolean isParachuteHeld = isParachuteHeldItem(player);

            if (!deployed && armor.getItem() instanceof ParachutePackItem && !isParachuteHeld) {
                player.getInventory().armor.set(EquipmentSlot.CHEST.getEntitySlotId(), ItemStack.EMPTY);
            } else if (isParachuteHeld && armor.isEmpty()) {
                ParachutePackItem pack = (ParachutePackItem) Registries.ITEM.get(new Identifier(ParachuteMod.MODID, ParachuteMod.PARACHUTE_PACK_ITEM_NAME));
                player.getInventory().armor.set(EquipmentSlot.CHEST.getEntitySlotId(), new ItemStack(pack));
            }
        }
    }

    @Unique
    private boolean isParachuteHeldItem(PlayerEntity player) {
        Iterable<ItemStack> items = player.getHandItems();
        for (ItemStack itemStack : items) {
            if (itemStack.getItem() instanceof ParachuteItem) {
                return true;
            }
        }
        return false;
    }

    // Handles the Automatic Activation Device, if the AAD is active
    // and the player is actually wearing the parachute deploy after
    // AAD_FALL_DISTANCE is reached.
    @Unique
    private void autoActivateDevice(PlayerEntity player) {
        boolean aadState = ParachuteItem.getAADState();

        if (aadState && !(player.getVehicle() instanceof ParachuteEntity)) {
            ItemStack heldItem = null;
            Iterable<ItemStack> heldEquipment = player.getHandItems();
            for (ItemStack itemStack : heldEquipment) {
                if (itemStack != null && itemStack.getItem() instanceof ParachuteItem) {
                    heldItem = itemStack;
                }
            }
            final double AAD_FALL_DISTANCE = 5.0;
            if (player.fallDistance > AAD_FALL_DISTANCE) {
                if (heldItem != null) {
                    ((ParachuteItem) heldItem.getItem()).deployParachute(player.getWorld(), player, heldItem);
                }
            }
        }
    }

}
