/*
 * Consts.java
 *  
 *  Copyright (c) 2020 Michael Sheppard
 *  
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.common;

// Constants used in both ParachuteEntity and ParachuteViewRenderMixin
public final class ModConstants {
    public static final double MAX_ALTITUDE = 256.0;

    public static final int MIN_LAVA_DISTANCE = 5;
    public static final int MAX_LAVA_DISTANCE = 56;

    public final static float HEAD_TURN_ANGLE = 120.0f;

    public final static double DECAY_MOMENTUM = 0.97;
    public final static double FORWARD_MOMENTUM = 0.015;
    public final static double BACK_MOMENTUM = 0.008;
    public final static double ROTATION_MOMENTUM = 0.35;
    public final static double SLIDE_MOMENTUM = 0.0025;
    public final static double TURN_MOMENTUM = 0.006;

    public static final float MAX_ROLL_ANGLE = 45.0f;

    public final static double DRIFT = 0.004; // value applied to motionY to descend or DRIFT downward
    public final static double ASCEND = DRIFT * -10.0; // -0.04 - value applied to motionY to ascend

    public final static float CHUTE_OFFSET = 2.5f; // player Y offset from parachute

    public static final int PARACHUTE_MAX_DAMAGE = 40;
}
