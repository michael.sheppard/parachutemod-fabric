/*
 * ParachuteEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.common.entity;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.*;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.*;
import net.minecraft.world.BlockLocating;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.parachutemod.common.ParachuteMod;
import net.parachutemod.common.ModConstants;
import org.joml.Vector3f;

public class ParachuteEntity extends Entity {
    private static final TrackedData<Integer> PARACHUTE_COLOR = DataTracker.registerData(ParachuteEntity.class, TrackedDataHandlerRegistry.INTEGER);

    private static float deltaRotation;
    private static double curLavaDistance;
    private static boolean ascendMode;

    private boolean leftKeyDown;
    private boolean rightKeyDown;
    private boolean forwardKeyDown;
    private boolean backKeyDown;
    private boolean jumpKeyDown;
    private int turnProgress;

    public static boolean leftTurn;
    public static boolean rightTurn;
    public static double forwardMotion;

    private double chuteX;
    private double chuteY;
    private double chuteZ;
    private double chuteYaw;
    private double chutePitch;

    // config variables
    private static boolean showRollEffect = ParachuteMod.CONFIG.getOrDefault("showRollEffect", false);
    private static final boolean rideInWater = ParachuteMod.CONFIG.getOrDefault("rideInWater", true);
    private static final boolean genContrails = ParachuteMod.CONFIG.getOrDefault("generateContrails", true);
    private static final boolean WASDSteering = ParachuteMod.CONFIG.getOrDefault("WASDSteering", true);
    private static final boolean driftOnly = ParachuteMod.CONFIG.getOrDefault("driftOnly", false);

    private static long lastDebounceTime = 0;
    private static final long DEBOUNCE_DELAY = 250; // milliseconds


    public ParachuteEntity(EntityType<? extends Entity> entityType, World world) {
        super(entityType, world);
        deltaRotation = 0.0f;
        curLavaDistance = ModConstants.MIN_LAVA_DISTANCE;
        setSilent(false);
    }

    public ParachuteEntity(World world, double x, double y, double z) {
        this(ParachuteMod.PARACHUTE, world);
        updatePosition(x, y, z);
        setVelocity(Vec3d.ZERO);
        prevX = x;
        prevY = y;
        prevZ = z;
    }

    public boolean getShowRollEffect() {
        return showRollEffect;
    }

    public static void toggleShowRollEffect() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            showRollEffect = !showRollEffect;
            lastDebounceTime = System.currentTimeMillis();
        }
    }

    @Override
    protected void initDataTracker() {
        dataTracker.startTracking(PARACHUTE_COLOR, 0);
    }

    @Override
    public Packet<ClientPlayPacketListener> createSpawnPacket() { // server to client packet
        return new EntitySpawnS2CPacket(this);
    }

    protected Vec3d positionInPortal(Direction.Axis portalAxis, BlockLocating.Rectangle portalRect) {
        return LivingEntity.positionInPortal(super.positionInPortal(portalAxis, portalRect));
    }

    protected float getEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return dimensions.height;
    }

    public boolean isCollidable() {
        return true;
    }

    @Override
    public void pushAwayFrom(Entity entity) {
        if (entity instanceof ParachuteEntity) {
            if (entity.getBoundingBox().minY < getBoundingBox().maxY) {
                super.pushAwayFrom(entity);
            }
        } else if (entity.getBoundingBox().minY <= getBoundingBox().minY) {
            super.pushAwayFrom(entity);
        }
    }

    // skydiver should hang when on the parachute and then
    // pick up legs when landing.
    // This is done via the BipedEntityModelMixin
    public boolean shouldRiderSit() {
        Entity skyDiver = getControllingPassenger();
        if (skyDiver != null) {
            return shouldRiderPickupLegs(skyDiver.getBlockPos());
        }
        return false;
    }

    // ray tracing for shouldRiderSit method
    private boolean shouldRiderPickupLegs(BlockPos bp) {
        Vec3d v1 = new Vec3d(bp.getX(), bp.getY(), bp.getZ());
        Vec3d v2 = new Vec3d(bp.getX(), bp.down(5).getY(), bp.getZ());
        HitResult traceResult = getWorld().raycast(new RaycastContext(v1, v2, RaycastContext.ShapeType.COLLIDER, RaycastContext.FluidHandling.ANY, this));
        if (traceResult.getType() == HitResult.Type.BLOCK) {
            return !getWorld().getBlockState(BlockPos.ofFloored(traceResult.getPos().x, traceResult.getPos().y, traceResult.getPos().z)).isAir();
        }
        return false;
    }

    public Direction getMovementDirection() {
        return this.getHorizontalFacing().rotateYClockwise();
    }

    protected Entity.MoveEffect getMoveEffect() {
        return MoveEffect.EVENTS;
    }

    protected Vector3f getPassengerAttachmentPos(Entity passenger, EntityDimensions dimensions, float scaleFactor) {
        return new Vector3f(0.0F, -ModConstants.CHUTE_OFFSET, 0.0f);
    }

    @Override
    protected void readCustomDataFromNbt(NbtCompound tag) {

    }

    @Override
    protected void writeCustomDataToNbt(NbtCompound tag) {

    }

    // updated via ParachuteUpdateInputsMixin class
    @Environment(EnvType.CLIENT)
    public void updateInputs(boolean leftKeyDown, boolean rightKeyDown, boolean forwardKeyDown, boolean backKeyDown, boolean jumpKeyDown) {
        this.leftKeyDown = leftKeyDown;
        this.rightKeyDown = rightKeyDown;
        this.forwardKeyDown = forwardKeyDown;
        this.backKeyDown = backKeyDown;
        this.jumpKeyDown = jumpKeyDown;
    }

    private void controlParachute() {
        if (this.hasPassengers()) {
            double motionFactor = 0.0f;

            if (forwardKeyDown) {
                motionFactor += ModConstants.FORWARD_MOMENTUM;
            }

            if (backKeyDown) {
                motionFactor -= ModConstants.BACK_MOMENTUM;
            }

            if (WASDSteering) {
                if (leftKeyDown) {
                    deltaRotation -= ModConstants.ROTATION_MOMENTUM;
                }
                if (rightKeyDown) {
                    deltaRotation += ModConstants.ROTATION_MOMENTUM;
                }

                // slight forward momentum while turning
                if (rightKeyDown != leftKeyDown && !forwardKeyDown && !backKeyDown) {
                    motionFactor += ModConstants.SLIDE_MOMENTUM;
                }
            }

            leftTurn = leftKeyDown;
            rightTurn = rightKeyDown;
            forwardMotion = motionFactor;

            if (!driftOnly) {
                ascendMode = jumpKeyDown;
            }

            if (WASDSteering) {
                float tempYaw = getYaw();
                tempYaw += deltaRotation;
                setYaw(tempYaw);
            } else {
                Entity skyDiver = getControllingPassenger();
                if (skyDiver instanceof LivingEntity pilot) {
                    setYaw(pilot.getYaw() + -pilot.sidewaysSpeed * 90.0f);
                }
            }

            PacketByteBuf modePacket = new PacketByteBuf(Unpooled.buffer());
            modePacket.writeBoolean(ascendMode);
            ClientPlayNetworking.send(ParachuteMod.C2S_ASCEND_MODE_MSG, modePacket);

            double motionY = currentDescentRate();
            double motionX = MathHelper.sin((float) Math.toRadians(-getYaw())) * motionFactor;
            double motionZ = MathHelper.cos((float) Math.toRadians(getYaw())) * motionFactor;
            setVelocity(getVelocity().add(motionX, motionY, motionZ));

            if (isBadWeather() && random.nextBoolean()) {
                applyTurbulence(getWorld().isThundering());
            }
        }
    }

    public void setParachuteColor(Color color) {
        dataTracker.set(PARACHUTE_COLOR, color.ordinal());
    }

    public Color getParachuteColor() {
        return Color.byID(dataTracker.get(PARACHUTE_COLOR));
    }

    public static void setAscendMode(boolean mode) {
        ascendMode = mode;
    }


    @Override
    public LivingEntity getControllingPassenger() {
        Entity passenger = this.getFirstPassenger();
        LivingEntity entity;
        if (passenger instanceof LivingEntity livingEntity) {
            entity = livingEntity;
        } else {
            entity = null;
        }

        return entity;
    }

    @Override
    public void tick() {
        Entity skyDiver = getControllingPassenger();
        // the player has pressed LSHIFT or been killed,
        // may be necessary for LSHIFT to kill the parachute
        if (skyDiver == null && !getWorld().isClient) { // server side
            remove(RemovalReason.KILLED);
            return;
        }

        // if the parachute gets submerged in water it drops
        if (isSubmergedInWater()) {
            remove(RemovalReason.KILLED);
        }

        generateContrails();

        prevX = getX();
        prevY = getY();
        prevZ = getZ();

        super.tick();
        updatePositionAndRotation();
        updateTrackedPosition(getX(), getY(), getZ());
        updateMotion();

        if (ascendMode) {
            // play the lift sound. kinda like a hot air balloon's burner sound
            if (skyDiver != null) {
                skyDiver.playSound(ParachuteMod.CHUTE_LIFT_SOUND_EVENT, 1.0f, 1.0F / (random.nextFloat() * 0.4F + 0.8F));
            }
        }

        setPos(getX(), getY(), getZ());
        // apply momentum/decay
        Vec3d curMotion = getVelocity();
        setVelocity(curMotion.x * ModConstants.DECAY_MOMENTUM, curMotion.y * (curMotion.y < 0.0 ? 0.96 : 0.98), curMotion.z * ModConstants.DECAY_MOMENTUM);
        deltaRotation *= 0.9;

        if (getWorld().isClient) {
            controlParachute();
        }
        // move the PARACHUTE with the motion equations applied
        move(MovementType.SELF, getVelocity());

        // something bad happened, somehow the skydiver was killed.
        if (!getWorld().isClient && skyDiver != null && !skyDiver.isAlive()) { // server side
            remove(RemovalReason.KILLED);
        }

        checkBlockCollision();
    }

    private void updateMotion() {
        if (isLogicalSideForUpdatingMovement()) {
            turnProgress = 0;
            updateTrackedPosition(getX(), getY(), getZ());
        }
        if (turnProgress > 0) {
            double dx = getX() + (chuteX - getX()) / (double) turnProgress;
            double dy = getY() + (chuteY - getY()) / (double) turnProgress;
            double dz = getZ() + (chuteZ - getZ()) / (double) turnProgress;
            double delta_r = MathHelper.wrapDegrees(chuteYaw - (double) getYaw());
            setYaw((float) ((double) getYaw() + delta_r / (double) turnProgress));
            setPitch((float) ((double) getYaw() + (chutePitch - (double) getPitch()) / (double) turnProgress));
            --turnProgress;
            setPos(dx, dy, dz);
            setRotation(getYaw(), getPitch());
        }
    }

    // check for bad weather, if the biome can rain or snow check to see
    // if it is raining, snowing or thundering.
    private boolean isBadWeather() {
        BlockPos bp = new BlockPos(getBlockPos());
        boolean canRain = getWorld().getBiome(bp).value().getPrecipitation(bp).equals(Biome.Precipitation.RAIN);
        boolean canSnow = getWorld().getBiome(bp).value().getPrecipitation(bp).equals(Biome.Precipitation.SNOW);
        return (canRain || canSnow) && (getWorld().isRaining() || getWorld().isThundering());
    }

    // determines the descent rate based on whether
    // the space bar has been pressed. weather and lava affect
    // the final result.
    private double currentDescentRate() {
        double descentRate;

        descentRate = calcHeatSourceThermals();

        descentRate += (getWorld().isRaining() ? 0.002 : getWorld().isThundering() ? 0.004 : 0.0);

        if (ascendMode) {
            descentRate = ModConstants.ASCEND;
        }

        if (getY() >= ModConstants.MAX_ALTITUDE) {
            descentRate = ModConstants.DRIFT;
        }

        return -descentRate;
    }

    // the following three methods detect lava|fire below the player
    // at up to MAX_LAVA_DISTANCE blocks. added logic to detect campfire.
    private boolean isHeatSource(BlockPos bp) {
        Block block = getWorld().getBlockState(bp).getBlock();
        return block == Blocks.FIRE || block == Blocks.LAVA || block == Blocks.CAMPFIRE;
    }

    private boolean isHeatSourceInRange(BlockPos bp) {
        Vec3d v1 = new Vec3d(getX(), getY(), getZ());
        Vec3d v2 = new Vec3d(bp.getX(), bp.getY(), bp.getZ());
        HitResult mop = getWorld().raycast(new RaycastContext(v1, v2, RaycastContext.ShapeType.COLLIDER, RaycastContext.FluidHandling.ANY, this));
        if (mop.getType() == HitResult.Type.BLOCK) {
            return isHeatSource(BlockPos.ofFloored(mop.getPos().x, mop.getPos().y, mop.getPos().z));
        }
        return false;
    }

    private double calcHeatSourceThermals() {
        double thermals = ModConstants.DRIFT;
        final double inc = 0.5;

        if (isHeatSourceInRange(new BlockPos(getBlockX(), getBlockY() - /*ModConstants.CHUTE_OFFSET - */ModConstants.MAX_LAVA_DISTANCE, getBlockZ()))) {
            curLavaDistance += inc;
            thermals = ModConstants.ASCEND;
            if (curLavaDistance >= ModConstants.MAX_LAVA_DISTANCE) {
                curLavaDistance = ModConstants.MIN_LAVA_DISTANCE;
                thermals = ModConstants.DRIFT;
            }
        } else {
            curLavaDistance = ModConstants.MIN_LAVA_DISTANCE;
        }
        return thermals;
    }

    // apply 'turbulence' in the form of a collision force
    private void applyTurbulence(boolean roughWeather) {
        double rmin = 0.1;
        double dPos = rmin + 0.9 * random.nextDouble();

        if (dPos >= 0.20) {
            double rmax = roughWeather ? 0.8 : 0.5;
            rmax = (random.nextInt(5) == 0) ? 1.2 : rmax;  // gusting
            double dX = rmin + (rmax - rmin) * random.nextDouble();
            double dY = rmin + 0.2 * random.nextDouble();
            double dZ = rmin + (rmax - rmin) * random.nextDouble();

            dPos = Math.sqrt(dPos);
            double dPosInv = 1.0 / dPos;

            dX /= dPos;
            dY /= dPos;
            dZ /= dPos;

            dPosInv = Math.min(dPosInv, 1.0);

            dX *= dPosInv;
            dY *= dPosInv;
            dZ *= dPosInv;

            dX *= 0.05;
            dY *= 0.05;
            dZ *= 0.05;

            if (random.nextBoolean()) {
                addVelocity(-dX, -dY, -dZ);
            } else {
                addVelocity(dX, dY, dZ);
            }
        }
    }

    // generate condensation trails at the trailing edge
    // of the parachute. Yes, I know that most parachutes
    // don't generate contrails (no engines), but most getWorlds
    // aren't made of block with cubic cows either. If you
    // like, you can think of the trails as chemtrails.
    private void generateContrails() {
        if (!genContrails) {
            return;
        }
        Vec3d motionVec = getVelocity();
        double velocity = Math.sqrt(motionVec.x * motionVec.x + motionVec.z * motionVec.z);
        double cosYaw = 2.25 * Math.cos(Math.toRadians(90.0 + getYaw()));
        double sinYaw = 2.25 * Math.sin(Math.toRadians(90.0 + getYaw()));

        for (int k = 0; (double) k < 1.0 + velocity; k++) {
            double sign = (random.nextInt(2) * 2 - 1) * 0.7;
            double x = getX() + (getX() - prevX) + cosYaw * -0.45 + sinYaw * sign;
            double y = getY() - 0.25;
            double z = getZ() + (getZ() - prevZ) + sinYaw * -0.45 - cosYaw * sign;

            if (velocity > 0.01) {
                getWorld().addParticle(ParticleTypes.CLOUD, x, y, z, motionVec.x, motionVec.y, motionVec.z);
            }
        }
    }

    public float getRidingOffset(Entity vehicle) {
        return -ModConstants.CHUTE_OFFSET;
    }

    @Override
    public void updatePassengerPosition(Entity passenger, Entity.PositionUpdater positionUpdater) {
        super.updatePassengerPosition(passenger, positionUpdater);
        passenger.setYaw(passenger.getYaw() + deltaRotation);
        passenger.setHeadYaw(passenger.getHeadYaw() + deltaRotation);
        clampPassengerYaw(passenger);
        // check for passenger collisions
        checkForPlayerCollisions(passenger);
    }

    protected void clampPassengerYaw(Entity entity) {
        entity.setBodyYaw(getYaw());
        float yaw = MathHelper.wrapDegrees(entity.getYaw() - getYaw());
        float yawClamped = MathHelper.clamp(yaw, -ModConstants.HEAD_TURN_ANGLE, ModConstants.HEAD_TURN_ANGLE);
        entity.prevYaw += yawClamped - yaw;
        entity.setYaw(entity.getYaw() + yawClamped - yaw);
        entity.setHeadYaw(entity.getYaw());
    }

    // check for player colliding with block below. stop riding if block is not air, water,
    // grass/vines, or snow/snow layers
    private void checkForPlayerCollisions(Entity passenger) {
        Box box = passenger.getBoundingBox();

        if (!getWorld().isClient && !getWorld().isSpaceEmpty(box)) {
            // if in water check dismount-in-water flag, also check for solid block below water
            if (passenger.isSubmergedInWater()) {
                if (!rideInWater) {
                    remove(RemovalReason.KILLED);
                } else {
                    BlockPos bp = new BlockPos(passenger.getBlockPos()).down(Math.round((float) box.minY));
                    if (!getWorld().getBlockState(bp.down(Math.round((float) box.minY))).isSolidBlock(getWorld(), bp)) {
                        return;
                    }
                }
            } else {
                BlockPos bp = new BlockPos(passenger.getBlockPos()).down(Math.round((float) box.minY));
                if (!getWorld().getBlockState(bp).isSolidBlock(getWorld(), bp)) {
                    return;
                } else if (getWorld().getBlockState(bp).isTransparent(getWorld(), bp)) {
                    remove(RemovalReason.KILLED);
                }
            }
            remove(RemovalReason.KILLED);
        }
    }

    protected void copyEntityData(Entity entity) {
        entity.setBodyYaw(getYaw());
        float f = MathHelper.wrapDegrees(entity.getYaw() - getYaw());
        float g = MathHelper.clamp(f, -105.0F, 105.0F);
        entity.prevYaw += g - f;
        entity.setYaw(entity.getYaw() + g - f);
        entity.setHeadYaw(entity.getYaw());
    }

    @Environment(EnvType.CLIENT)
    @Override
    public void onPassengerLookAround(Entity passenger) {
        copyEntityData(passenger);
    }

    @Override
    protected boolean canAddPassenger(Entity passenger) {
        return getPassengerList().isEmpty();
    }

    @Environment(EnvType.CLIENT)
    @Override
    public void updateTrackedPositionAndAngles(double x, double y, double z, float yaw, float pitch, int interpolationSteps) {
        chuteX = x;
        chuteY = y;
        chuteZ = z;
        chuteYaw = yaw;
        chutePitch = pitch;
        turnProgress = 10;
    }

    private void updatePositionAndRotation() {
        if (isLogicalSideForUpdatingMovement()) {
            turnProgress = 0;
            updateTrackedPosition(getX(), getY(), getZ());
        }

        if (turnProgress > 0) {
            lerpPosAndRotation(turnProgress, chuteX, chuteY, chuteZ, chuteYaw, chutePitch);
            --turnProgress;
        }
    }

    public enum Color {
        BLACK("black"),
        BLUE("blue"),
        BROWN("brown"),
        CYAN("cyan"),
        GRAY("gray"),
        GREEN("green"),
        LIGHT_BLUE("light_blue"),
        LIGHT_GRAY("light_gray"),
        LIME("lime"),
        MAGENTA("magenta"),
        ORANGE("orange"),
        PINK("pink"),
        PURPLE("purple"),
        RED("red"),
        WHITE("white"),
        YELLOW("yellow"),
        CAMO("camo"),
        RAINBOW("rainbow");

        private final String name;

        Color(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }

        public static Color byID(int id) {
            Color[] chuteColors = values();
            if (id < 0 || id >= chuteColors.length) {
                id = 0;
            }
            return chuteColors[id];
        }
    }

}
