/*
 * ParachuteItem.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package net.parachutemod.common.item;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.item.Item;
import net.parachutemod.client.sound.ParachuteFlyingSound;
import net.parachutemod.common.ModConstants;
import net.parachutemod.common.ParachuteMod;
import net.parachutemod.common.entity.ParachuteEntity;

import java.util.List;
import java.util.Random;


public class ParachuteItem extends Item {
    private static boolean aadState = ParachuteMod.CONFIG.getOrDefault("initialAADState", true);
    private static final boolean singleUse = ParachuteMod.CONFIG.getOrDefault("singleUse", false);
    private final ParachuteEntity.Color color;
    private final Random RANDOM = new Random();

    public ParachuteItem(ParachuteEntity.Color color_val, Settings settings) {
        super(settings);
        color = color_val;
    }

    public TypedActionResult<ItemStack> use(World world, PlayerEntity playerEntity, Hand hand) {
        ItemStack itemstack = playerEntity.getStackInHand(hand);
        if (isFalling(playerEntity) && !playerEntity.hasVehicle()) {
            return deployParachute(world, playerEntity, itemstack);
        } else { // toggle the AAD state
            return toggleAAD(itemstack, world, playerEntity);
        }
    }

    public TypedActionResult<ItemStack> deployParachute(World world, PlayerEntity playerEntity, ItemStack itemstack) {
        ParachuteEntity chute = new ParachuteEntity(world, playerEntity.getX(), playerEntity.getY() + ModConstants.CHUTE_OFFSET, playerEntity.getZ());
        chute.setParachuteColor(color);
        chute.setYaw(playerEntity.getYaw());

        final float volume = 1.0F;
        chute.playSound(ParachuteMod.CHUTE_OPEN_SOUND_EVENT, volume, audio_pitch());

        // parachute spawning is done via ParachuteEntitySpawnMixin
        if (!world.isClient) {
            world.spawnEntity(chute);
            playerEntity.startRiding(chute);
        } else {
            playFlyingSound(MinecraftClient.getInstance().player);
        }

        playerEntity.incrementStat(ParachuteMod.PARACHUTE_DEPLOYMENTS); // update parachute deployed statistics

        Iterable<ItemStack> heldEquipment = playerEntity.getHandItems();
        for (ItemStack itemStack : heldEquipment) {
            if (itemStack != null && itemStack.getItem() instanceof ParachuteItem) {
                itemstack = itemStack;
            }
        }
        if (!playerEntity.getAbilities().creativeMode || itemstack.isDamageable()) {
            if (singleUse) {
                itemstack.decrement(1);
            }
            int damage = itemstack.getDamage();
            itemstack.setDamage(damage + 1);
            if (damage >= itemstack.getMaxDamage()) {
                itemstack.decrement(1);
            }
        }
        return new TypedActionResult<>(ActionResult.CONSUME, itemstack);
    }

    private TypedActionResult<ItemStack> toggleAAD(ItemStack itemstack, World world, PlayerEntity playerEntity) {
        if (playerEntity != null) {
            boolean curAADState = aadState;
            if (!world.isClient) { // server side
                curAADState = !curAADState;
                aadState = curAADState;

                playerEntity.sendMessage(Text.translatable(curAADState ? "aad.tooltip.active" : "aad.tooltip.inactive").formatted(curAADState ? Formatting.GREEN : Formatting.RED), false);

                PacketByteBuf buffer = new PacketByteBuf(Unpooled.buffer());
                buffer.writeBoolean(aadState);
                ServerPlayNetworking.send((ServerPlayerEntity) playerEntity, ParachuteMod.S2C_AAD_MSG, buffer);
            } else { // client side
                world.playSoundAtBlockCenter(new BlockPos(playerEntity.getBlockX(), playerEntity.getBlockY()/* + ModConstants.CHUTE_OFFSET*/, playerEntity.getBlockZ()),
                        SoundEvents.UI_BUTTON_CLICK.value(), SoundCategory.MASTER, 1.0f, 1.0f, false);
            }
            return new TypedActionResult<>(ActionResult.CONSUME, itemstack);
        }
        return new TypedActionResult<>(ActionResult.FAIL, itemstack);
    }

    @Override
    public void appendTooltip(ItemStack itemStack, World world, List<Text> tooltip, TooltipContext tooltipContext) {
        boolean state = getAADState();
<<<<<<< HEAD
        int deploymentsLeft = ModConstants.PARACHUTE_MAX_DAMAGE - itemStack.getDamage();
        tooltip.add(new TranslatableText(state ? "aad.tooltip.active" : "aad.tooltip.inactive").formatted(state ? Formatting.GREEN : Formatting.RED));
        tooltip.add(new TranslatableText("Deployments Left: " + deploymentsLeft).formatted(Formatting.AQUA));
=======
        tooltip.add(Text.translatable(state ? "aad.tooltip.active" : "aad.tooltip.inactive").formatted(state ? Formatting.GREEN : Formatting.RED));
>>>>>>> 1.20.2
    }

    private boolean isFalling(PlayerEntity player) {
        return (player.fallDistance > 0.0F && !player.isOnGround() && !player.isHoldingOntoLadder());
    }

    private float audio_pitch() {
        return 1.0F / (RANDOM.nextFloat() * 0.4F + 0.8F);
    }

    @Override
    public boolean canRepair(ItemStack toRepair, ItemStack repair) {
        return Items.STRING.equals(repair.getItem());
    }

    @Environment(EnvType.CLIENT)
    private void playFlyingSound(ClientPlayerEntity playerEntity) {
        MinecraftClient.getInstance().getSoundManager().play(new ParachuteFlyingSound(playerEntity));
    }

    public static boolean getAADState() {
        return aadState;
    }

    public static void setAADState(boolean state) {
        aadState = state;
    }
}
