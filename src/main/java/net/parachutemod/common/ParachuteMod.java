/*
 * ParachuteMod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.common;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.*;
import net.minecraft.item.*;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.sound.SoundEvent;
import net.minecraft.stat.StatFormatter;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.parachutemod.common.config.SimpleConfig;
import net.parachutemod.common.entity.ParachuteEntity;
import net.parachutemod.common.item.CustomPackMaterial;
import net.parachutemod.common.item.ParachuteItem;
import net.parachutemod.common.item.ParachutePackItem;

import java.util.logging.Logger;


@SuppressWarnings("unused")
public class ParachuteMod implements ModInitializer {
    public static final String MODID = "parachutemod";
    public static final String PARACHUTE_NAME = "parachute";
    public static final String ASCEND_MODE_PACKET_NAME = "ascend_mode_packet_name";
    public static final String AAD_PACKET_NAME = "aad_packet_name";

    public static SimpleConfig CONFIG = SimpleConfig.of(ParachuteMod.MODID).provider(ParachuteMod::provider).request();

    public static final Identifier PARACHUTE_DEPLOYMENTS = new Identifier(MODID, "parachute_deployments");

    public static final Logger LOGGER = Logger.getLogger(ParachuteMod.MODID);

    public static final String BLACK_PARACHUTE_NAME = "black_parachute";
    public static final String BLUE_PARACHUTE_NAME = "blue_parachute";
    public static final String BROWN_PARACHUTE_NAME = "brown_parachute";
    public static final String CYAN_PARACHUTE_NAME = "cyan_parachute";
    public static final String GRAY_PARACHUTE_NAME = "gray_parachute";
    public static final String GREEN_PARACHUTE_NAME = "green_parachute";
    public static final String LIGHT_BLUE_PARACHUTE_NAME = "light_blue_parachute";
    public static final String LIGHT_GRAY_PARACHUTE_NAME = "light_gray_parachute";
    public static final String LIME_PARACHUTE_NAME = "lime_parachute";
    public static final String MAGENTA_PARACHUTE_NAME = "magenta_parachute";
    public static final String ORANGE_PARACHUTE_NAME = "orange_parachute";
    public static final String PINK_PARACHUTE_NAME = "pink_parachute";
    public static final String PURPLE_PARACHUTE_NAME = "purple_parachute";
    public static final String RED_PARACHUTE_NAME = "red_parachute";
    public static final String WHITE_PARACHUTE_NAME = "white_parachute";
    public static final String YELLOW_PARACHUTE_NAME = "yellow_parachute";
    public static final String CAMO_PARACHUTE_NAME = "camo_parachute";
    public static final String RAINBOW_PARACHUTE_NAME = "rainbow_parachute";

    public static final String PARACHUTE_PACK_ITEM_NAME = "parachute_pack";

    public static final String CHUTE_OPEN_SOUND = "chuteopen";
    public static final String CHUTE_LIFT_SOUND = "lift";

    public static final Identifier CHUTE_OPEN_SOUND_ID = new Identifier(ParachuteMod.MODID, CHUTE_OPEN_SOUND);
    public static final SoundEvent CHUTE_OPEN_SOUND_EVENT = SoundEvent.of(CHUTE_OPEN_SOUND_ID);

    public static final Identifier CHUTE_LIFT_SOUND_ID = new Identifier(ParachuteMod.MODID, CHUTE_LIFT_SOUND);
    public static final SoundEvent CHUTE_LIFT_SOUND_EVENT = SoundEvent.of(CHUTE_LIFT_SOUND_ID);

    public static final ArmorMaterial CUSTOM_PACK_MATERIAL = new CustomPackMaterial();
    public static final Item PARACHUTE_PACK_ITEM = new ParachutePackItem(CUSTOM_PACK_MATERIAL, ArmorItem.Type.CHESTPLATE, (new FabricItemSettings().maxCount(1)));

    public static final ParachuteItem PARACHUTE_ITEM_BLACK = new ParachuteItem(ParachuteEntity.Color.BLACK, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_BLUE = new ParachuteItem(ParachuteEntity.Color.BLUE, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_BROWN = new ParachuteItem(ParachuteEntity.Color.BROWN, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_CYAN = new ParachuteItem(ParachuteEntity.Color.CYAN, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_GRAY = new ParachuteItem(ParachuteEntity.Color.GRAY, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_GREEN = new ParachuteItem(ParachuteEntity.Color.GREEN, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_LIGHT_BLUE = new ParachuteItem(ParachuteEntity.Color.LIGHT_BLUE, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_LIGHT_GRAY = new ParachuteItem(ParachuteEntity.Color.LIGHT_GRAY, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_LIME = new ParachuteItem(ParachuteEntity.Color.LIME, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_MAGENTA = new ParachuteItem(ParachuteEntity.Color.MAGENTA, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_ORANGE = new ParachuteItem(ParachuteEntity.Color.ORANGE, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_PINK = new ParachuteItem(ParachuteEntity.Color.PINK, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_PURPLE = new ParachuteItem(ParachuteEntity.Color.PURPLE, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_RED = new ParachuteItem(ParachuteEntity.Color.RED, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_WHITE = new ParachuteItem(ParachuteEntity.Color.WHITE, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_YELLOW = new ParachuteItem(ParachuteEntity.Color.YELLOW, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_CAMO = new ParachuteItem(ParachuteEntity.Color.CAMO, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));
    public static final ParachuteItem PARACHUTE_ITEM_RAINBOW = new ParachuteItem(ParachuteEntity.Color.RAINBOW, new FabricItemSettings().maxDamage(ModConstants.PARACHUTE_MAX_DAMAGE));

    public static final EntityType<Entity> PARACHUTE = Registry.register(
            Registries.ENTITY_TYPE,
            new Identifier(ParachuteMod.MODID, PARACHUTE_NAME),
            FabricEntityTypeBuilder.create(SpawnGroup.MISC, ParachuteEntity::new)
                    .dimensions(EntityDimensions.fixed(3.25f, (1.0f / 16.0f)))
                    .trackedUpdateRate(3)
                    .trackRangeBlocks(32)
                    .forceTrackedVelocityUpdates(true)
                    .build()
    );

    public static final Identifier C2S_ASCEND_MODE_MSG = new Identifier(ParachuteMod.MODID, ParachuteMod.ASCEND_MODE_PACKET_NAME);
    public static final Identifier S2C_AAD_MSG = new Identifier(ParachuteMod.MODID, ParachuteMod.AAD_PACKET_NAME);

    private static final RegistryKey<ItemGroup> PARACHUTE_GROUP = RegistryKey.of(RegistryKeys.ITEM_GROUP, new Identifier(MODID, PARACHUTE_NAME));

    @Override
    public void onInitialize() {
        Registry.register(Registries.ITEM_GROUP, PARACHUTE_GROUP, FabricItemGroup.builder()
                .displayName(Text.literal(PARACHUTE_NAME))
                .icon(() -> new ItemStack(PARACHUTE_ITEM_RAINBOW))
                .build());

        ItemGroupEvents.modifyEntriesEvent(PARACHUTE_GROUP).register(content -> {
            content.add(PARACHUTE_ITEM_BLACK);
            content.add(PARACHUTE_ITEM_BROWN);
            content.add(PARACHUTE_ITEM_CYAN);
            content.add(PARACHUTE_ITEM_GRAY);
            content.add(PARACHUTE_ITEM_GREEN);
            content.add(PARACHUTE_ITEM_LIGHT_BLUE);
            content.add(PARACHUTE_ITEM_LIGHT_GRAY);
            content.add(PARACHUTE_ITEM_LIME);
            content.add(PARACHUTE_ITEM_MAGENTA);
            content.add(PARACHUTE_ITEM_ORANGE);
            content.add(PARACHUTE_ITEM_PINK);
            content.add(PARACHUTE_ITEM_PURPLE);
            content.add(PARACHUTE_ITEM_RED);
            content.add(PARACHUTE_ITEM_WHITE);
            content.add(PARACHUTE_ITEM_YELLOW);
            content.add(PARACHUTE_ITEM_CAMO);
            content.add(PARACHUTE_ITEM_RAINBOW);
        });
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, PARACHUTE_PACK_ITEM_NAME), PARACHUTE_PACK_ITEM);

        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, BLACK_PARACHUTE_NAME), PARACHUTE_ITEM_BLACK);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, BLUE_PARACHUTE_NAME), PARACHUTE_ITEM_BLUE);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, BROWN_PARACHUTE_NAME), PARACHUTE_ITEM_BROWN);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, CYAN_PARACHUTE_NAME), PARACHUTE_ITEM_CYAN);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, GRAY_PARACHUTE_NAME), PARACHUTE_ITEM_GRAY);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, GREEN_PARACHUTE_NAME), PARACHUTE_ITEM_GREEN);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, LIGHT_BLUE_PARACHUTE_NAME), PARACHUTE_ITEM_LIGHT_BLUE);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, LIGHT_GRAY_PARACHUTE_NAME), PARACHUTE_ITEM_LIGHT_GRAY);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, LIME_PARACHUTE_NAME), PARACHUTE_ITEM_LIME);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, MAGENTA_PARACHUTE_NAME), PARACHUTE_ITEM_MAGENTA);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, ORANGE_PARACHUTE_NAME), PARACHUTE_ITEM_ORANGE);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, PINK_PARACHUTE_NAME), PARACHUTE_ITEM_PINK);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, PURPLE_PARACHUTE_NAME), PARACHUTE_ITEM_PURPLE);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, RED_PARACHUTE_NAME), PARACHUTE_ITEM_RED);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, WHITE_PARACHUTE_NAME), PARACHUTE_ITEM_WHITE);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, YELLOW_PARACHUTE_NAME), PARACHUTE_ITEM_YELLOW);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, CAMO_PARACHUTE_NAME), PARACHUTE_ITEM_CAMO);
        Registry.register(Registries.ITEM, new Identifier(ParachuteMod.MODID, RAINBOW_PARACHUTE_NAME), PARACHUTE_ITEM_RAINBOW);

        Registry.register(Registries.SOUND_EVENT, CHUTE_OPEN_SOUND_ID, CHUTE_OPEN_SOUND_EVENT);
        Registry.register(Registries.SOUND_EVENT, CHUTE_LIFT_SOUND_ID, CHUTE_LIFT_SOUND_EVENT);

        ServerPlayNetworking.registerGlobalReceiver(C2S_ASCEND_MODE_MSG, (server, serverPlayer, networkHandler, packetByteBuf, packetSender) -> {
            boolean mode = packetByteBuf.readBoolean();
            server.execute(() -> ParachuteEntity.setAscendMode(mode));
        });

        Registry.register(Registries.CUSTOM_STAT, "parachute_deployments", PARACHUTE_DEPLOYMENTS);
        Stats.CUSTOM.getOrCreateStat(PARACHUTE_DEPLOYMENTS, StatFormatter.DEFAULT);
    }

    private static String provider(String filename) {
        return """
                # ParachuteMod Configuration File
                #################################
                showRollEffect=false
                rideInWater=true
                generateContrails=true
                initialAADState=true
                WASDSteering=true
                singleUse=false
                driftOnly=false
                HUDOpacity=0.75
                """;
    }
}
