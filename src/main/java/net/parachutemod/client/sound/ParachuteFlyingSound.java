/*
 * ParachuteFlyingSound.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.client.sound;


import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.sound.MovingSoundInstance;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.parachutemod.common.entity.ParachuteEntity;

@Environment(EnvType.CLIENT)
public class ParachuteFlyingSound extends MovingSoundInstance {
    private final ClientPlayerEntity player;
    private int time;

    public ParachuteFlyingSound(ClientPlayerEntity player) {
        super(SoundEvents.ITEM_ELYTRA_FLYING, SoundCategory.PLAYERS, net.minecraft.util.math.random.Random.create());
        this.player = player;
        volume = 0.5f;
        repeat = true;
        repeatDelay = 0;
    }

    @Override
    public void tick() {
        ++time;
        boolean isRidingParachute = player.getVehicle() instanceof ParachuteEntity;

        if (!player.isRemoved() && (time <= 20 || (player.hasVehicle() && isRidingParachute))) {
            x = (float)(player.getX() - player.prevX);
            y = (float)(player.getY() - player.prevY);
            z = (float)(player.getZ() - player.prevZ);
            float velocity = MathHelper.sqrt((float) (x * x + y * y + z * z));
            if ((double) velocity >= 0.01) {
                volume = MathHelper.clamp(velocity, 0.0f, 1.0f);
            } else {
                volume = 0.1f;
            }
            if (time < 20) {
                volume = 0.1f;
            } else if (time < 40) {
                volume = (float) ((double) volume * ((double) (time - 20) / 20.0));
            }
            if (volume > 0.8f) {
                pitch = 1.0f + (volume - 0.8f);
            } else {
                pitch = 1.0f;
            }
        } else {
            setDone();
        }
    }
}
