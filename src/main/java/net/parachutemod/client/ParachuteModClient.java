/*
 * ParachuteModClient.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
//import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
//import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;
import net.parachutemod.client.model.ParachuteModel;
import net.parachutemod.client.renderer.HUDCompassRenderer;
import net.parachutemod.client.renderer.ParachuteRenderer;
import net.parachutemod.common.entity.ParachuteEntity;
import net.parachutemod.common.item.ParachuteItem;
import net.parachutemod.common.ParachuteMod;
import org.lwjgl.glfw.GLFW;


@Environment(EnvType.CLIENT)
public class ParachuteModClient implements ClientModInitializer {
    private static KeyBinding TOGGLE_HUD_VISIBILITY;
    private static KeyBinding TOGGLE_ROLL_EFFECT;
    private static final String CATEGORY = "key.category.parachutemod.general";
//    private final HUDCompassRenderer hud = new HUDCompassRenderer(MinecraftClient.getInstance());
    public static final EntityModelLayer modelLayer = new EntityModelLayer(new Identifier(ParachuteMod.MODID, "textures/block/camo_parachute.png"), ParachuteMod.CAMO_PARACHUTE_NAME);

    @SuppressWarnings("unchecked")
    @Override
    public void onInitializeClient() {
        EntityModelLayerRegistry.registerModelLayer(modelLayer, ParachuteModel::getTexturedModelData);
        EntityRendererRegistry.register(ParachuteMod.PARACHUTE, ctx -> (EntityRenderer) new ParachuteRenderer(ctx));

        ClientPlayNetworking.registerGlobalReceiver(ParachuteMod.S2C_AAD_MSG, ((server, networkHandler, packetByteBuf, sender) ->
                ParachuteItem.setAADState(packetByteBuf.readBoolean())
        ));

//        HudRenderCallback.EVENT.register((hud::renderHUD));

        TOGGLE_HUD_VISIBILITY = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.parachutemod.toggle_hud", GLFW.GLFW_KEY_H, CATEGORY));
        TOGGLE_ROLL_EFFECT = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.parachutemod.toggle_roll", GLFW.GLFW_KEY_M, CATEGORY));

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_HUD_VISIBILITY.isPressed()) {
                HUDCompassRenderer.toggleHUDVisibility();
            }
        });

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_ROLL_EFFECT.isPressed()) {
                ParachuteEntity.toggleShowRollEffect();
            }
        });
    }
}
