/*
 * ParachuteRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.client.renderer;

import com.google.common.collect.ImmutableMap;
import net.minecraft.client.render.*;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory.Context;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import com.mojang.datafixers.util.Pair;
import net.minecraft.util.math.RotationAxis;
import net.parachutemod.client.ParachuteModClient;
import net.parachutemod.common.ParachuteMod;
import net.parachutemod.common.entity.ParachuteEntity;
import net.parachutemod.client.model.ParachuteModel;
import org.joml.Matrix4f;

import java.util.Map;
import java.util.stream.Stream;

public class ParachuteRenderer extends EntityRenderer<ParachuteEntity> {
    private final Map<ParachuteEntity.Color, Pair<Identifier, ParachuteModel>> texturesAndModel;

    public ParachuteRenderer(Context ctx) {
        super(ctx);
        shadowRadius = 0.0f;
        texturesAndModel = Stream.of(ParachuteEntity.Color.values()).collect(ImmutableMap.toImmutableMap((color) -> color, (color) ->
                Pair.of(new Identifier(ParachuteMod.MODID, "textures/block/" + color.getName() + "_parachute.png"), new ParachuteModel(ctx.getPart(ParachuteModClient.modelLayer)))));
    }

    @Override
    public Identifier getTexture(ParachuteEntity entity) {
        return (Identifier) ((Pair)texturesAndModel.get(entity.getParachuteColor())).getFirst();
    }

    @Override
    public void render(ParachuteEntity parachuteEntity, float rotationYaw, float partialTicks, MatrixStack matrixStack, VertexConsumerProvider vertexConsumers, int packedLight) {
        matrixStack.push();
        matrixStack.translate(0.0D, 0.0D, 0.0D);
        matrixStack.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(-rotationYaw));

        Pair<Identifier, ParachuteModel> pair = texturesAndModel.get(parachuteEntity.getParachuteColor());
        Identifier identifier = pair.getFirst();
        ParachuteModel parachuteModel = pair.getSecond();
        VertexConsumer vertexConsumer = vertexConsumers.getBuffer(parachuteModel.getLayer(identifier));
        parachuteModel.render(matrixStack, vertexConsumer, packedLight, OverlayTexture.DEFAULT_UV, 1.0F, 1.0F, 1.0F, 1.0F);

        if (dispatcher.camera.isThirdPerson()) {
            renderParachuteCords(matrixStack, vertexConsumers);
        }
        matrixStack.pop();

        super.render(parachuteEntity, rotationYaw, partialTicks, matrixStack, vertexConsumers, packedLight);
    }

    public void renderParachuteCords(MatrixStack matrixStack, VertexConsumerProvider typeBuffer) {
        final float SCALE = 0.0625f;
        int cordColor = 90;

        // six section parachute
        final float[] x = { // left/right
                -34f, -3f, -34f, -3f, -20f, -3f,
                -20f, -3f, 34f, 3f, 34f, 3f,
                20f, 3f, 20f, 3f, -8f, -3f,
                -8f, -3f, 8f, 3f, 8f, 3f
        };
        final float[] y = { // up/down
                0.52f, 1.25f, 0.52f, 1.25f, 0.2f, 1.25f,
                0.2f, 1.25f, 0.52f, 1.25f, 0.52f, 1.25f,
                0.2f, 1.25f, 0.2f, 1.25f, 0.05f, 1.25f,
                0.05f, 1.25f, 0.05f, 1.25f, 0.05f, 1.25f
        };
        final float[] z = { // front/back
                -8f, 0f, 8f, 0f, -8f, 0f,
                8f, 0f, -8f, 0f, 8f, 0f,
                -8f, 0f, 8f, 0f, -8f, 0f,
                8f, 0f, -8f, 0f, 8f, 0f
        };

        matrixStack.push();

        matrixStack.scale(SCALE, -1.0f, SCALE);
        VertexConsumer consumerProvider = typeBuffer.getBuffer(RenderLayer.LINES);
        Matrix4f matrix4f = matrixStack.peek().getPositionMatrix();

        for (int k = 0; k < x.length; k++) {
            consumerProvider.vertex(matrix4f, x[k], y[k], z[k]).color(cordColor, cordColor, cordColor, 255).normal(1.0f, 1.0f, 1.0f).next();
        }

        matrixStack.pop();
    }

}
