/*
 * HUDCompassRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.client.renderer;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RotationAxis;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.parachutemod.common.entity.ParachuteEntity;
import net.parachutemod.common.ParachuteMod;


@Environment(EnvType.CLIENT)
public class HUDCompassRenderer {
    private static final Identifier COMPASS_TEXTURE = new Identifier(ParachuteMod.MODID + ":" + "textures/gui/hud-compass.png");
    private static final Identifier HOME_TEXTURE = new Identifier(ParachuteMod.MODID + ":" + "textures/gui/hud-home.png");
    private static final Identifier RETICULE_RED_TEXTURE = new Identifier(ParachuteMod.MODID + ":" + "textures/gui/hud-reticule_red.png");
    private static final Identifier RETICULE_GREEN_TEXTURE = new Identifier(ParachuteMod.MODID + ":" + "textures/gui/hud-reticule_green.png");
    private static final Identifier RETICULE_YELLOW_TEXTURE = new Identifier(ParachuteMod.MODID + ":" + "textures/gui/hud-reticule_yellow.png");
    private static final Identifier BACKGROUND_TEXTURE = new Identifier(ParachuteMod.MODID + ":" + "textures/gui/hud-background.png");
    private static final Identifier NIGHT_TEXTURE = new Identifier(ParachuteMod.MODID + ":" + "textures/gui/hud-night.png");

    private static final int MOON_RISE = 12600;
    private static final int SUN_RISE = 22900;
    private static final int MAX_TICKS = 24000;

    // colors with 0.75% opacity to match the hud images
    private static final int COLOR_RED = 0xbfff0000;
    private static final int COLOR_GREEN = 0xbf00ff00;
    private static final int COLOR_YELLOW = 0xbfffff00;

    private final MinecraftClient client;

    private static double altitude;
    public static boolean isVisible = true;

    private static int count = 0;

    private static final int HUD_WIDTH = 256;
    private static final int HUD_HEIGHT = 256;
    private static final int Y_PADDING = 20;
    private static final int X_PADDING = 20;

    private static long lastDebounceTime = 0;
    private static final long DEBOUNCE_DELAY = 250; // milliseconds
    private final double hudOpacity = ParachuteMod.CONFIG.getOrDefault("HUDOpacity", 0.75);
    private final String hudPosition = ParachuteMod.CONFIG.getOrDefault("HUDPosition", "left");

    private String alt;
    private String compass;
    private String dist;
    private String vel;

    private MatrixStack matrixStack;

    public HUDCompassRenderer(MinecraftClient client) {
        this.client = client;
    }

    @SuppressWarnings("unused")
    public void renderHUD(DrawContext ctx, float tickDelta) {
        matrixStack = ctx.getMatrices();
        
        if ((client.player != null && client.player.isOnGround())) {
            return;
        }
        if (!isVisible || !client.options.getFullscreen().getValue()) {
            return;
        }
        PlayerEntity player = client.player;
        if (player == null) {
            return;
        }

        if (client.world != null) {
            TextRenderer fontRenderer = client.textRenderer;

            int width = client.getWindow().getWidth();
            int hudX = (width - HUD_WIDTH) - X_PADDING;

            int textX = hudX + (HUD_WIDTH / 2);
            int textY = Y_PADDING + (HUD_HEIGHT / 2);

            Entity chute = null;
            if (player.getVehicle() instanceof ParachuteEntity) {
                chute = player.getVehicle();
            }
            if (chute == null) {
                return;
            }

            BlockPos skyDiverPos = BlockPos.ofFloored(player.getX(), (player.getY() - (player.getHeight() / 2)), player.getZ());

            altitude = getCurrentAltitude(skyDiverPos);
            double homeDir = getHomeDirection(chute.getYaw());
            double distance = getHomeDistance();
            double compassHeading = calcCompassHeading(chute.getYaw());
            double velocity = getVelocity(chute);

            matrixStack.push();

            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();

            matrixStack.scale(0.25f, 0.25f, 0.25f);

            // draw the background
            if (isNightTime()) {
                drawTextureFixed(ctx, NIGHT_TEXTURE, hudX);
            }
            drawTextureFixed(ctx, BACKGROUND_TEXTURE, hudX);

            // draw the compass ring
            drawTextureWithRotation(ctx, (float) -compassHeading, COMPASS_TEXTURE, hudX);

            // draw the home direction ring
            drawTextureWithRotation(ctx, (float) homeDir, HOME_TEXTURE, hudX);

            // draw the "where the hell is the front of the parachute" color-coded reticule
            // red = not front facing, yellow =  +/-10 degrees, green = +/-2 degrees
            float playerLook = MathHelper.wrapDegrees(player.getHeadYaw() - chute.getYaw());
            if (playerLook <= 2.5 && playerLook >= -2.5) {
                drawTextureFixed(ctx, RETICULE_GREEN_TEXTURE, hudX);
            } else if (playerLook <= 15.0 && playerLook >= -15.0) {
                drawTextureFixed(ctx, RETICULE_YELLOW_TEXTURE, hudX);
            } else {
                drawTextureFixed(ctx, RETICULE_RED_TEXTURE, hudX);
            }

            // dampen the update (20 ticks/second modulo 10 is about 1/2 second updates)
            if (count % 10 == 0) {
                alt = formatBold(altitude);
                compass = formatBold(compassHeading);
                dist = formatBold(distance);
                vel = formatBold(velocity);
            }

            count++;

            // scale text up to 50%
            matrixStack.scale(2.0f, 2.0f, 2.0f);
            // scale the text coordinates as well
            textX /= 2;
            textY /= 2;

            int hFont = fontRenderer.fontHeight;
            // draw the compass heading text
            ctx.drawText(fontRenderer, compass, textX - fontRenderer.getWidth(compass) / 2, textY - (hFont * 2) - 2, COLOR_GREEN, true);

            // draw the altitude text
            ctx.drawText(fontRenderer, alt, textX - fontRenderer.getWidth(alt) / 2, textY - hFont, colorAltitude(), true);

            // draw the distance to the home/spawn point text
            ctx.drawText(fontRenderer, dist, textX - fontRenderer.getWidth(dist) / 2, textY + 2, COLOR_GREEN, true);

            // draw velocity
            ctx.drawText(fontRenderer, vel, textX - fontRenderer.getWidth(vel) / 2, textY + hFont + 4, COLOR_GREEN, true);

            RenderSystem.disableBlend();

            matrixStack.pop();
        }
    }

    private boolean isNightTime() {
        long ticks = (client.world != null ? client.world.getTimeOfDay() : 0) % MAX_TICKS;
        return ticks > MOON_RISE && ticks < SUN_RISE;
    }

    // draw a fixed texture
    private void drawTextureFixed(DrawContext ctx, Identifier texture, int screenX) {
        matrixStack.push();

        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, (float) hudOpacity);
        ctx.drawTexture(texture, screenX, Y_PADDING, 0, 0, HUD_WIDTH, HUD_HEIGHT);

        matrixStack.pop();
    }

    // draw a rotating texture
    private void drawTextureWithRotation(DrawContext ctx, float degrees, Identifier texture, int screenX) {
        matrixStack.push();

        float tx = screenX + (HUD_WIDTH / 2.0f);
        float ty = Y_PADDING + (HUD_HEIGHT / 2.0f);
        // translate to center and rotate
        matrixStack.translate(tx, ty, 0);
        matrixStack.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(degrees));
        matrixStack.translate(-tx, -ty, 0);

        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, (float) hudOpacity);
        ctx.drawTexture(texture, screenX, Y_PADDING, 0, 0, HUD_WIDTH, HUD_HEIGHT);

        matrixStack.pop();
    }

    // Minecraft font style codes
    // §k	Obfuscated
    // §l	Bold
    // §m	Strikethrough
    // §n	Underline
    // §o	Italic
    // §r	Reset
    private String formatBold(double d) {
        return String.format("§l%.1f", d);
    }

    private double calcCompassHeading(double yaw) {
        return (((yaw + 180.0) % 360) + 360) % 360;
    }

    // difference angle in degrees the chute is facing from the home point.
    // zero degrees means the chute is facing the home point.
    // the home point can be either the world spawn point or a waypoint
    // set by the player in the config.
    private double getHomeDirection(double yaw) {
        BlockPos blockpos = client.world != null ? client.world.getSpawnPos() : new BlockPos(0, 0, 0);
        Vec3d v = client.player != null ? client.player.getPos() : new Vec3d(0.0, 0.0, 0.0);
        double delta = Math.atan2(blockpos.getZ() - v.z, blockpos.getX() - v.x);
        double relAngle = delta - Math.toRadians(yaw);
        return MathHelper.wrapDegrees(Math.toDegrees(relAngle) - 90.0); // degrees
    }

    // Thanks to Pythagoras we can calculate the distance to home/spawn
    private double getHomeDistance() {
        BlockPos blockpos = client.world != null ? client.world.getSpawnPos() : new BlockPos(0, 0, 0);
        Vec3d v = client.player != null ? client.player.getPos() : new Vec3d(0.0, 0.0, 0.0);
        double a = Math.pow(blockpos.getZ() - v.z, 2);
        double b = Math.pow(blockpos.getX() - v.x, 2);
        return Math.sqrt(a + b);
    }

    private int colorAltitude() {
        return altitude <= 10.0 ? COLOR_RED : altitude <= 15.0 ? COLOR_YELLOW : COLOR_GREEN;
    }

    // calculate altitude in meters above ground. starting at the entity
    // count down until a non-air block is encountered.
    // only allow altitude calculations in the surface world
    // return a weirdly random number if in nether or end.
    private double getCurrentAltitude(BlockPos entityPos) {
        if (client.world != null && client.world.getRegistryKey().getValue().equals(World.OVERWORLD.getValue())) {
            BlockPos blockPos = new BlockPos(entityPos.getX(), entityPos.getY(), entityPos.getZ());
            while (client.world.isAir(blockPos)) {
                blockPos = blockPos.down();
            }
            // calculate the entity's current altitude above the ground
            return entityPos.getY() - blockPos.getY();
        }
        return 1000.0 * (client.world != null ? client.world.random.nextGaussian() : 0);
    }

    private double getVelocity(Entity chute) {
        Vec3d vel = chute.getVelocity();
        return ((vel.x * vel.x) + (vel.z * vel.z)) * 100.0;
    }

    // toggles HUD visibility using a user defined key, 'H' is default
    public static void toggleHUDVisibility() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            isVisible = !isVisible;
            lastDebounceTime = System.currentTimeMillis();
        }
    }

}
