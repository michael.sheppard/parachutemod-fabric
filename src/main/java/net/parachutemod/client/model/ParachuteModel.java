/*
 * ParachuteModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.parachutemod.client.model;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.*;
import net.minecraft.client.render.entity.model.CompositeEntityModel;
import net.parachutemod.common.entity.ParachuteEntity;


@Environment(EnvType.CLIENT)
public class ParachuteModel extends CompositeEntityModel<ParachuteEntity> {

    private final ImmutableList<ModelPart> parachute_sections;

    public ParachuteModel(ModelPart root) {
        parachute_sections = ImmutableList.of(root.getChild("one"), root.getChild("two"), root.getChild("three"), root.getChild("four"), root.getChild("five"), root.getChild("six"));
    }

    public static TexturedModelData getTexturedModelData() {
        final float d2r = (float) Math.toRadians(1.0);
        ModelData modelData = new ModelData();
        ModelPartData modelPartData = modelData.getRoot();

        modelPartData.addChild("one", ModelPartBuilder.create().uv(6, 0).cuboid(-5f, -8.25f, -8f, 4, 2, 16), ModelTransform.of(-36F, 0F, 0F, 0.0F, 0.0F, 45.0F * d2r));
        modelPartData.addChild("two", ModelPartBuilder.create().uv(0, 0).cuboid(-32F, 1.6F, -8F, 16, 2, 16), ModelTransform.of(0F, 0F, 0F, 0.0F, 0.0F, 15.0F * d2r));
        modelPartData.addChild("three", ModelPartBuilder.create().uv(0, 0).cuboid(-16F, -0.5F, -8F, 16, 2, 16), ModelTransform.of(0F, 0F, 0F, 0.0F, 0.0F, 7.5F * d2r));
        modelPartData.addChild("four", ModelPartBuilder.create().uv(0, 0).cuboid(0F, -0.5F, -8F, 16, 2, 16), ModelTransform.of(0F, 0F, 0F, 0.0F, 0.0F, -7.5F * d2r));
        modelPartData.addChild("five", ModelPartBuilder.create().uv(0, 0).cuboid(16F, 1.6F, -8F, 16, 2, 16), ModelTransform.of(0F, 0F, 0F, 0.0F, 0.0F, -15.0F * d2r));
        modelPartData.addChild("six", ModelPartBuilder.create().uv(6, 0).cuboid(1f, -8.25f, -8f, 4, 2, 16), ModelTransform.of(36F, 0F, 0F, 0.0F, 0.0F, -45.0F * d2r));

        return TexturedModelData.of(modelData, 16, 16);
    }

    @Override
    public void setAngles(ParachuteEntity entity, float limbAngle, float limbDistance, float animationProgress, float headYaw, float headPitch) {}

    @Override
    public ImmutableList<ModelPart> getParts() {
        return parachute_sections;
    }

}
